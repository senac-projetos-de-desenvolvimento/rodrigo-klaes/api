import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import generateRandomCode from 'src/helpers/generateCode';
import { Repository } from 'typeorm';
import Code from './entity/code.entity'
import { VerifyCodeParams } from './type/code.type';

@Injectable()
export class CodeService {
  constructor(
    @InjectRepository(Code)
    private codeRepository: Repository<Code>
  ){ }

  async generate(user_id: string, length = 6): Promise<string>{
    const code = generateRandomCode(length)
    const codeId = await this.codeRepository.save({user_id, code})
    
    if(!codeId){
      throw new HttpException('Erro ao gerar código.', 500)
    }

    return code
  }

  async verifyCode(data: VerifyCodeParams): Promise<Code>{
    const code = await this.codeRepository.findOne({ where: data})

    if(!code){
      throw new HttpException('Código não encontrado.', 404)
    }

    return code
  }

  async deleteCode(id): Promise<boolean>{
    await this.codeRepository.delete({id})
    return true
  }
}
