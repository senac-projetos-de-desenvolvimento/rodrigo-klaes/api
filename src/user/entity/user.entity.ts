import BaseEntity from '../../base/base.entity';
import { Column, Entity, CreateDateColumn, UpdateDateColumn, OneToMany, JoinColumn, JoinTable, ManyToMany } from 'typeorm';
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';
import Category from '../../category/entity/category.entity';
import Order from '../../order/entity/order.entity';
import Signature from 'src/signature/entity/signature.entity';
import Plan from 'src/plan/entity/plan.entity';
import Townhouse from 'src/townhouse/entity/townhouse.entity';
import Address from 'src/address/entity/address.entity';

export enum UserRole {
    ADMIN = 'admin',
    CLIENTE = 'client',
}

@Entity('users')
export default class User extends BaseEntity {
  @Column()
  @ApiModelProperty()
  readonly name: string;

  @Column({unique: true})
  @ApiModelProperty()
  readonly email: string;

  @Column()
  readonly password: string;

  @Column({unique: true})
  @ApiModelProperty()
  readonly cpf: string;

  @Column()
  @ApiModelProperty()
  readonly phone: string

  @Column({nullable: true})
  readonly token: string;

  @Column({default: false})
  @ApiModelPropertyOptional()
  readonly is_checked: boolean;

  @Column({
    type: "enum",
    enum: UserRole,
  })  
  @ApiModelProperty()
  readonly role: UserRole;

  @OneToMany(type => Order, order => order.user)
  readonly orders: Order[]

  @OneToMany(type => Signature, signature => signature.user)
  readonly signatures: Signature[]

  @OneToMany(type => Address, address => address.user)
  readonly addresses: Address[]

  @ManyToMany(type => Plan)
  @JoinTable({
        name: "signatures",
        joinColumn: {
          name: "user_id",
          referencedColumnName: "id"            
        },
        inverseJoinColumn: {
          name: "plan_id",
          referencedColumnName: "id"
        }
    })
    readonly plans: Plan[]
    
  @ManyToMany(type => Townhouse)
  @JoinTable({
        name: "addresses",
        joinColumn: {
          name: "user_id",
          referencedColumnName: "id"            
        },
        inverseJoinColumn: {
          name: "townhouse_id",
          referencedColumnName: "id"
        }
    })
    readonly townhouses: Townhouse[]
}