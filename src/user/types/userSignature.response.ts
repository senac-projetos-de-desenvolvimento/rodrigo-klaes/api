export default class UserSignatureResponse{
    user_id: string
    username: string
    plan_name: string
    price: number
    plan_id: string
    plan_description: string
    id: string
}
