import { Controller, Get, Param, Query, Res, UseGuards } from '@nestjs/common';
import { BaseController } from '../base/base.controller';
import Schedule from './entity/schedule.entity';
import { ScheduleService } from './schedule.service';
import { ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { QueryParams } from 'src/base/type/getAllParams';

@ApiUseTags('schedules')
@Controller('schedules')
export class ScheduleController extends BaseController<Schedule> {
    constructor(private readonly scheduleService: ScheduleService){
        super(scheduleService)
    }

  @Get('/:id')
  @UseGuards(JwtAuthGuard)
	@ApiResponse({ status: 200, description: 'Ok'})
	async getWithRelations(@Param('id') id: string): Promise<Schedule> {
    return await this.scheduleService.get(id, ['order', 'user', 'status'])
  }
  
  @Get()
  @ApiResponse({ status: 200, description: 'Ok', type: Array<Schedule>() })
  @UseGuards(JwtAuthGuard)
  async findAll(@Query() query: QueryParams): Promise<ResponseType> {
    return await this.scheduleService.getAll({ query, relations: ['order', 'user', 'status'] });
  }

  @Get('/next/deliveries')
  @ApiResponse({ status: 200, description: 'Ok', type: Array<Schedule>() })
  @UseGuards(JwtAuthGuard)
  async nextDeliveries(@Query() query: QueryParams): Promise<ResponseType> {
    return await this.scheduleService.nextDeliveries({ query, relations: ['order', 'user', 'status'] });
  }

  @Get('/export/delivery')
	@ApiResponse({ status: 200, description: 'Ok'})
	async export(@Res() res): Promise<any> {
    const resp = await this.scheduleService.exportNextDeliveries()
    
    return res.download(resp)
  }

}
