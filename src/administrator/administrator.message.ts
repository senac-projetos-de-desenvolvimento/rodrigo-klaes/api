export default {
  NOT_FOUND: {
      message: 'Administrador não encontrado',
      status: 404
  }
}