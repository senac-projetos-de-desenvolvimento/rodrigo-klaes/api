import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import IMailParams from './type/mail.params';

@Injectable()
export  default class MailService {
  constructor(private readonly mailerService: MailerService) {}
  
  public sendEmail(mailConfig: IMailParams): void {
    try {
      this
        .mailerService
        .sendMail({
          to: mailConfig.to, // list of receivers
          from: mailConfig.from, // sender address
          subject: mailConfig.subject, // Subject line
          text: mailConfig.text, // plaintext body
          html: mailConfig.html, // HTML body content
        })
    } catch (error) {
      throw new error
    }
  }
    
  
}