export default {
    NOT_FOUND: {
        message: 'Produto da ordem não encontrado.',
        status: 404
    },
    NOT_CREATED: {
        message: 'Não foi possível adicionar produto na ordem.',
        status: 500
    }
}