import { Entity, Column, ManyToOne, JoinColumn } from "typeorm";
import { ApiModelProperty } from "@nestjs/swagger";
import BaseEntity from "../../base/base.entity";
import Order from "src/order/entity/order.entity";
import Product from "src/product/entity/product.entity";

@Entity('order_products')
export default class OrderProduct extends BaseEntity {
    @Column({unique: false})
    @ApiModelProperty()
    readonly product_id: string

    @Column({unique: false})
    @ApiModelProperty()
    readonly order_id: string

    @Column({type: "float"})
    @ApiModelProperty()
    readonly price: number
    
    @Column({type: "float"})
    @ApiModelProperty()
    readonly amount: number

    @ManyToOne(type => Order, order => order.orderProduct)
    @JoinColumn({name: 'order_id'})
    readonly order: Order

    @ManyToOne(type => Product, product => product.orderProduct)
    @JoinColumn({name: 'product_id'})
    readonly product: Product
}