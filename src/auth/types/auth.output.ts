export default class AuthOutput {
    access_token: string;
    user_id: string;
    role: string
}