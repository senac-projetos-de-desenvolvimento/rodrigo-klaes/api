import { Controller, Get } from '@nestjs/common';
import { BaseController } from '../base/base.controller';
import Category from './entity/category.entity';
import { CategoryService } from './category.service';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

@ApiUseTags('categories')
@Controller('categories')
export class CategoryController extends BaseController<Category> {
  constructor(private readonly categoryService: CategoryService) {
    super(categoryService);
  }

  @Get('/products')
  @ApiResponse({ status: 200, description: 'Ok' })
  async findAllWithRelations(): Promise<ResponseType> {
    return await this.categoryService.getAll({ relations: ['products'] });
  }
}
