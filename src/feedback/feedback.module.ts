import { FeedbackService } from './feedback.service';
import { FeedbackController } from './feedback.controller';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import Feedback from './entity/feedback.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Feedback])],
    controllers: [ FeedbackController ],
    providers: [ FeedbackService ],
    exports: [ FeedbackService ],
})
export class FeedbackModule {}
