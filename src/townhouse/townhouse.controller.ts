import { Controller } from '@nestjs/common';
import { BaseController } from '../base/base.controller';
import Townhouse from './entity/townhouse.entity';
import { TownhouseService } from './townhouse.service';
import { ApiUseTags } from '@nestjs/swagger';

@ApiUseTags('townhouses')
@Controller('townhouses')
export class TownhouseController extends BaseController<Townhouse> {
  constructor(private readonly townhouseService: TownhouseService){
    super(townhouseService)
  }
}
