export default {
  NOT_FOUND: {
      message: 'Endereço não encontrado',
      status: 404
  }
}