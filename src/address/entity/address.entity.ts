import { Entity, Column, ManyToOne, JoinColumn } from "typeorm";
import BaseEntity from "../../base/base.entity";
import { ApiModelProperty } from "@nestjs/swagger";
import User from "src/user/entity/user.entity";
import Townhouse from "src/townhouse/entity/townhouse.entity";

@Entity('addresses')
export default class Address extends BaseEntity {
    @Column()
    @ApiModelProperty()
    readonly user_id: string

    @Column()
    @ApiModelProperty()
    readonly townhouse_id: string

    @Column()
    @ApiModelProperty()
    readonly name: string

    @ManyToOne(type => Townhouse)
    @JoinColumn({name: 'townhouse_id'})
    readonly townhouse: Townhouse

    
    @ManyToOne(type => User, user => user.orders)
    @JoinColumn({name: 'user_id'})
    readonly user: User
}