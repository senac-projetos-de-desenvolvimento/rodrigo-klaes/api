import { BadGatewayException, Injectable } from '@nestjs/common';
import { BaseService } from '../base/base.service';
import Product from './entity/product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import messages from './product.message'
import uploadImage from 'src/helpers/uploadFIle.helper';

@Injectable()
export class ProductService extends BaseService<Product>{
    constructor(
        @InjectRepository(Product)
        private readonly productRepository: Repository<Product>
    ) {
        super(productRepository, messages)
    }

    async createWithFile(entity: Product, file: any): Promise<string> {
        try {    
    
          const image = await uploadImage(file.path)
            console.log({...entity, image});
            
          const created = await this.productRepository.save({...entity, image});
          return created.id;
        } catch (error) {
          throw new BadGatewayException(error);
        }
      }
    
}
