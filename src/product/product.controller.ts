import { Body, Controller, Get, Post, Query, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { BaseController } from '../base/base.controller';
import Product from './entity/product.entity';
import { ProductService } from './product.service';
import { ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { QueryParams } from 'src/base/type/getAllParams';
import { FileInterceptor } from '@nestjs/platform-express';

@ApiUseTags('products')
@Controller('products')
export class ProductController extends BaseController<Product> {
  constructor(private readonly productService: ProductService) {
    super(productService);
  }

  @Get('/category')
  @ApiResponse({ status: 200, description: 'Ok' })
  @UseGuards(JwtAuthGuard)
  async findAllWithRelations(@Query() query: QueryParams): Promise<ResponseType> {
    return await this.productService.getAll({ relations: ['category'], query });
  }

  @UseInterceptors(FileInterceptor('file'))
  @Post('/file')
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 400, description: 'Bad Request.' })
  async createWithFile(@Body() entity: Product, @UploadedFile() file: any): Promise<string> {   
    try {
      console.log(file);
      
      return await this.productService.createWithFile(JSON.parse(JSON.stringify(entity)), file);
    } catch (error) {
      console.log(error);
      
    }         
  }
}
