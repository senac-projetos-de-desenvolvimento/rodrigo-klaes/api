import { getAllParams } from './type/getAllParams';

export interface IBaseService<T> {
  getAll(getAllParams: getAllParams): Promise<ResponseType>;
  get(id: string, relations?: string[]): Promise<T>;
  update(id: string, entity: T): Promise<T>;
  create(entity: T): Promise<string>;
  delete(id: string);
}
