export default class ResponseType<T> {
    total: number
    page: number
    size: number
    totalPage: number
    data: T[] | []
}