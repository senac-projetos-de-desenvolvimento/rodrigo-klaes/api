import {
  Get,
  Post,
  Delete,
  Put,
  Body,
  Param,
  UseGuards,
  Query,
} from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { IBaseService } from './IBase.service';
import BaseEntity from './base.entity';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { QueryParams } from './type/getAllParams';

export class BaseController<T extends BaseEntity> {
  constructor(private readonly IBaseService: IBaseService<T>) {}

  @Get()
  @ApiResponse({ status: 200, description: 'Ok', type: Array<T>() })
  @UseGuards(JwtAuthGuard)
  async findAll(@Query() query: QueryParams): Promise<ResponseType> {
    return await this.IBaseService.getAll({ query });
  }

  @Get(':id')
  @ApiResponse({ status: 200, description: 'Entity retrieved successfully.' })
  @ApiResponse({ status: 404, description: 'Entity does not exist' })
  @UseGuards(JwtAuthGuard)
  async findById(@Param('id') id: string): Promise<T> {
    return await this.IBaseService.get(id);
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({ status: 400, description: 'Bad Request.' })
  @UseGuards(JwtAuthGuard)
  async create(@Body() entity: T): Promise<string> {
    return await this.IBaseService.create(entity);
  }

  @Delete(':id')
  @ApiResponse({ status: 200, description: 'Entity deleted successfully.' })
  @ApiResponse({ status: 400, description: 'Bad Request.' })
  @UseGuards(JwtAuthGuard)
  async delete(@Param('id') id: string): Promise<string> {
    await this.IBaseService.delete(id);
    return 'true';
  }

  @Put(':id')
  @ApiResponse({ status: 400, description: 'Bad Request.' })
  @ApiResponse({ status: 200, description: 'Entity deleted successfully.' })
  @UseGuards(JwtAuthGuard)
  async update(@Body() entity: T, @Param() id: string): Promise<T> {
    return await this.IBaseService.update(id, entity);
  }
}
