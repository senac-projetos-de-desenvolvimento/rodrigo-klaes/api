import { Injectable } from '@nestjs/common';
import { BaseService } from '../base/base.service';
import Plan from './entity/plan.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import messages from './plan.message'

@Injectable()
export class PlanService extends BaseService<Plan> {
    constructor(
        @InjectRepository(Plan)
        private readonly planRepository: Repository<Plan>
    ) {
        super(planRepository, messages)
    }
}

